#!/bin/bash

# resources:
# https://akomljen.com/get-kubernetes-logs-with-efk-stack-in-5-minutes


# add the akomljen-charts repo
helm repo add akomljen-charts https://raw.githubusercontent.com/komljen/helm-charts/master/charts/

# install the es operator
helm install --name es-operaor --namespace logging akomljen-charts/elasticsearch-operator

# install efk using an umbrella helm chart
helm install --name efk --namespace logging akomljen-charts/efk
 
# use port forwarding to access kabana
# kubectl port-forward efk-kibana-6cf88598b6-xlkv2 5601 -n logging